package com.activiti.extension.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "${activiti-ext.scan.packages}")
public class JerseyJaxrsActivitiExtSpringComponentScanner {

}
